const $ = require( "jquery" )
const utils = require("./utils")
const axios = require('axios')
const { detect } = require('detect-browser');
const browser = detect();

class ArticleObserver { 

  constructor () { 
    this.scrolled = 0 
    this.leftToScroll = 0
    this.scrolledPercentage = 0 
    this.pageClicks = []
    this.history = {scroll:0}  // Containes Last Values History 
    this.onFocus = 0  

    setInterval(()=> {
      if (document.hasFocus())
      ++this.onFocus; 
    },1000)

  } 

  prepareHTML() {
    // Set ID's For Elements to be
    const siteElements  = $("body *"); 
    let number = 0 ;   
    siteElements.map(function (k) { 
        $(this).attr("data-track", number++)  
    })
    return true
  }

  setScrollListener () { 
    document.addEventListener("scroll", () => { 
        let docsWindow       = $(document.defaultView)  // This is the window holding the document
        let docsWindowHeight = docsWindow.height()      // The viewport of the wrapper window
        let scrolled        = $(document).scrollTop()   // How much we scrolled already, in the viewport
        let docHeight        = $(document).height()     // This is the full document height.  
        let leftToScroll = docHeight - (docsWindowHeight + scrolled)
        let scrolledPercentage = 100.0 * (1 - leftToScroll / docHeight)

        this.scrolled = scrolled
        this.leftToScroll = leftToScroll
        this.scrolledPercentage =  scrolledPercentage  
      }); 
  }

  async getUserInfo () {
    try {
      let respText = await axios.get("https://www.cloudflare.com/cdn-cgi/trace")
      //let resplocalisation = await axios.get("https://api.ipdata.co/?api-key=test") 
      respText = respText.data 
      this.ip = respText.substring(respText.indexOf('ip=')+3,respText.indexOf('ts=')).trim()
      this.useragent = browser.name + '-' + browser.version
      this.useros = browser.os
      //this.country = resplocalisation.data.country_name 
    } 
    catch (error) { 
      console.log(error);
    } 
  }
  
  async handShakeInfos () {
    await this.getUserInfo() 
    let d = new Date()
    return {
      ip : this.ip,
      //country : this.country,
      browser : this.useragent,
      os : this.useros,
      url : window.location.href.indexOf('?fbclid') > 0 ? encodeURI(window.location.href.substring(window.location.href.indexOf('?fbclid'),-1)) : encodeURI(window.location.href) ,
      userid : utils.getMatomoCookie(),
      windowwidth : $(window).width(),
      windowheight: $(window).height(),
      date : d.getFullYear()+'-' + ( parseInt( d.getMonth() ) + 1 ) + '-'+d.getUTCDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds(), 
      referrer : encodeURI(document.referrer),
      pagetype : $("meta[property='indepth:type']").attr('content'),
      pageauthors : $("meta[property='indepth:authors']").attr('content'),
      pagepublishdate : $("meta[property='indepth:publishdate']").attr('content'),
      pagetitle : $('title').text()
    }
  }

  
  setClickListener ()  { 
    document.addEventListener("click", (event) => { 
      target = $(event.target)  

      documentClick = {
          element :  utils.domElementToObject(event.target)  ,
          parents : [] , 
          docX : event.clientX,
          docY : event.clientY,
          scrolledpx : this.scrolled
      } 

      if (target.parents('a').length) {
        documentClick.parents.push(  utils.domElementToObject(target.parents('a')[0])  );
      }

      if (target.parents('svg').length) {
        documentClick.parents.push( utils.domElementToObject(target.parents('svg')[0] ) ); 
      }

      if (target.parents('button').length) {
        documentClick.parents.push( utils.domElementToObject( target.parents('button')[0] ) ); 
      } 

      this.pageClicks.push(documentClick)  
    });

  }
 
  getInfos() {    
    let infos = new Object() 
    if ( this.pageClicks.length > 0 )
    infos.pageClicks = this.pageClicks 
    infos.scroll = this.scrolledPercentage 
    if (this.onFocus>0) 
    infos.onFocus = this.onFocus
    this.initValues() 
    return infos    
  }

  initValues() { 
    this.pageClicks = [] 
    this.onFocus = 0
  }
 
}

module.exports = ArticleObserver
