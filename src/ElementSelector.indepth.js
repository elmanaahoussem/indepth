class ElementSelector {

    constructor () { 
        this.elements = [] 
    }  
    
    scanArticle () {
        $('blockquote , svg , img , h1 , h2 , h3 , h4 , h5 , h6 , button, audio,video').map( (e,l) => { this.elements.push(l)} )  
        return this.elements
    }

}

module.exports = ElementSelector 


