const axios = require('axios')
const endPointURL = 'http://localhost:3000/' //'https://plesk.indepth.inku.be/'
const utils = require("./utils")
const $ = require( "jquery" )

class Sender { 

    constructor (ao,elements) { 
        this.ao = ao 
        this.elements = elements
        this.visitId = null 
        this.onLeave()
    }

    async handShake () { 
        let visit = await this.ao.handShakeInfos()
        try {
            let resp = await axios.post( endPointURL + 'visit/add' , visit)
            this.visitId =  resp.data._id
        } catch (e) {
            console.log(e)
        } 
    }

    onLeave () {
        window.onbeforeunload = () => { 
            this.heartBeat(document.activeElement.href)
            return undefined
        } 
    }

    async heartBeat(nexturl) {
        let behaviors = {
            visitid : this.visitId,
            timestamp : Date.now(),
            articleInfos : this.ao.getInfos(),
            events : [] ,
            nexturl : nexturl || null
        }

        this.elements.map((el) => {
            if (el.gotNews()) { 
                behavior = {
                    element : utils.domElementToObject(el.element),
                    events : el.getInfos()
                }
                if ( behavior.events.length > 0  )
                    behaviors.events.push(behavior)
            }
        }) 
        //console.log(behaviors)
        try {
            resp = await axios.post(endPointURL + 'behavior/add' , {behaviors : JSON.stringify(behaviors) , crossDomain: true})
            behaviors = null ;
        }catch (e) {
            console.log(e)
        } 
    }


}

module.exports = Sender