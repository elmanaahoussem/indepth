const $ = require( "jquery" )
const utils = require("./utils")
 
class Bot {

    constructor() { 
        this.actions = []
        this.scrolledPercentage = 0 
        this.pageClicks = []
        this.actions = []
        this.doneActions = []
        this.setListeners();
    }

    setListeners () {
        document.addEventListener("scroll", () => { 
            let docsWindow       = $(document.defaultView)  // This is the window holding the document
            let docsWindowHeight = docsWindow.height()      // The viewport of the wrapper window
            let scrolled        = $(document).scrollTop()   // How much we scrolled already, in the viewport
            let docHeight        = $(document).height()     // This is the full document height.  
            let leftToScroll = docHeight - (docsWindowHeight + scrolled)
            let scrolledPercentage = 100.0 * (1 - leftToScroll / docHeight) 
            this.scrolledPercentage =  Math.max( this.scrolledPercentage , scrolledPercentage )   
        }); 
    }

    scrollDown(n) {
        $('html, body').animate({ scrollTop: $(document).scrollTop() + n }, 2000);
    }

    scrollUp(n) {
        $('html, body').animate({ scrollTop: $(document).scrollTop() - n }, 2000);
    }

    scrollTo (element) {
        $('html, body').animate({ scrollTop: $(element).offset().top }, 2000);
    }

    click(element) { 
        $(element).click()
        target = $(element)   
        documentClick = {
            element :  utils.domElementToObject(target)  ,
            parents : []  
        }   
        if (target.parents('a').length) {
          documentClick.parents.push(  utils.domElementToObject(target.parents('a')[0])  );
        }  
        if (target.parents('svg').length) {
          documentClick.parents.push( utils.domElementToObject(target.parents('svg')[0] ) ); 
        } 
        if (target.parents('button').length) {
          documentClick.parents.push( utils.domElementToObject( target.parents('button')[0] ) ); 
        }  
        this.pageClicks.push(documentClick)  
    }

    play (element) {
        $(element)[0].play()
    }

    pause (element) {
        $(element)[0].pause()
    }

    skipTo (element,value) {
        $(element)[0].currentTime = value
    }

    fullscreen(element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) { /* Firefox */
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) { /* IE/Edge */
            element.msRequestFullscreen();
        }
    }

    exitFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {  /* Firefox */
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {     /* IE/Edge */
            document.msExitFullscreen();
        }
    }
 
    script(action,value,time,element=null) {
        this.actions.push({
            action : action,
            time : time,
            element : element ,
            value : value , 
        })
    }

    playScript() {
        offsetTime = 0 ;
        this.actions.map((e) => {
            setTimeout(()=> {
                switch (e.action) {
                    case 'scrollup'  : this.scrollUp(e.value); this.logAction('scrollup',e.value,'document'); break ; 
                    case 'scrolldown': this.scrollDown(e.value); this.logAction('scrolldown',e.value,'document'); break ; 
                    case 'scrollto': this.scrollTo(e.element); this.logAction('scrollto','N/A', utils.domElementToObject(e.element) ); break ; 
                    case 'click'  : this.click(e.element); this.logAction('click','N/A', utils.domElementToObject(e.element) ); break ; 
                    case 'pause'  : this.pause(e.element); this.logAction('pause','N/A', utils.domElementToObject(e.element) ); break ; 
                    case 'play'  : this.play(e.element); this.logAction('play','N/A', utils.domElementToObject(e.element) ); break ; 
                    case 'skipto'  : this.skipTo(e.element,e.value); this.logAction('skipto',e.value, utils.domElementToObject(e.element) ); break;
                    case 'onfullscreen'  : this.fullscreen(e.element); this.logAction('onfullscreen','N/A', utils.domElementToObject(e.element) ); break;
                    case 'exitFullscreen'  : this.exitFullscreen(e.element); this.logAction('exitFullscreen','N/A', utils.domElementToObject(e.element) ); break; 
                }
            }, offsetTime + e.time )
            offsetTime += e.time 
        })  
        setTimeout(() => { this.getActionsLog() } , offsetTime)
    }

    logAction(action,value,element){
        this.doneActions.push({
            action : action,
            value : value,
            element: element
        }) 
    }

    getActionsLog() {
        this.doneActions.push({
            documentScroll : this.scrolledPercentage + '%',
            pageClicks : this.pageClicks
        }) 
        exportData = 'data:text/csv;charset=utf-8,';
        exportData += JSON.stringify(this.doneActions);
        encodedUri = encodeURI(exportData);
        newWindow = window.open(encodedUri); 
    }
 
}
 
module.exports = Bot