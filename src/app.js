const $ = require( "jquery" )

import ElementObserver from './ElementObserver' 
import ArticleObserver from './ArticleObserver'
import ElementSelector from './ElementSelector' 
import Sender from './Sender'
 
$(document).ready(function(){
 
    const AO = new ArticleObserver() 
    AO.prepareHTML()        // Adds Track IDs to Elements
    AO.setScrollListener()  // Listen to Scroll on Full Document
    AO.setClickListener()   // Listen to Clicks on All Document

    const documentBody = new ElementObserver(document.body,AO)  
    //documentBody.mouseTrackOnly(1000)   // Tracks Only Mouse Mouvement on the body
    
    const elementSelector = new ElementSelector() 
    let elements = elementSelector.scanArticle()

    let observersTable = []
    observersTable.push(documentBody)
    elements.map( (element) => {  
        let observer = new ElementObserver(element,AO,{mouseTrack : false , clickTrack : false , watchTime : true , watchScroll : true })
        observer.watch()
        observersTable.push(observer)
    }) 

    const sender = new Sender(AO,observersTable) 
    sender.handShake();
    setInterval(() => {  
        sender.heartBeat();
    },10000)  
})  