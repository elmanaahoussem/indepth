const $ = require( "jquery" )
const utils = require("./utils")

class ElementObserver { 
  constructor (element,ao, args={mouseTrack : true , clickTrack : true , watchTime : true , watchScroll : true } ) {   
    
    this.element = element       // The Element To Observe
    this.ao = ao                 // Article Observer 
    this.args = args             // Element Parameters to Enable Watchers

    this.timer = null            // Timer event Listener on Element
    this.watchScroll = null      // Scroll event Listener on Element
    this.watchMouse = null       // Mouse Event Listener on Element
 
    this.clickX = 0              // Last X of the mouse Click on the Element
    this.clickY = 0              // Last Y of the mouse Click on the Element 
    this.mouseX = 0              // Last X of the mouse on the Element
    this.mouseY = 0              // Last Y of the mouse on the Element 
    
    this.watchTime = 0           // Time of the Element on screen 
    this.scrolledPercentage = 0  // Max Scroll on the Element
    this.mouseClicks = []        // Contains Locations of mouse clicks
    this.mouseHistory = []       // Logs Mouse Mouvements 
    
    this.fullScreen = false       // Is Full Screened Or Not
    this.isMuted = false          // Is muted
    this.playTimeListener = null // Play Time event Listener 
    this.playTime = 0            // total play time for AUDIO & Video
    this.playHistory = []        // Contains Play History For AUDIO & VIDEO

    // Contains Last Values History 
    this.history = { scroll: 0 , capture : { x : 0 , y : 0 } }

    this.init()
  } 
  
  
  init() {  
    // Init Intersection Observer 
    this.observerFull = new IntersectionObserver( (entries) => { 
      if(entries[0].isIntersecting === true)
      {
        this.onScreen()
      }
      else {
        this.offScreen()
      } 
      }, { threshold: [0.5] }   // Threshold  1 = 100% 
    );  

    // Init Intersection Observer 
    this.observerIntersect = new IntersectionObserver( (entries) => { 
        if(entries[0].isIntersecting === true)
        {
          this.onIntersect()
        }
        else {
          this.offIntersect()
        } 
        }, { threshold: [0.01] }   // Threshold  1 = 10% 
    );   

    if ( this.element.tagName == "AUDIO" || this.element.tagName == "VIDEO") 
         this.initPlayingEvents()
         
  }

  initPlayingEvents() { 


    $(this.element).on('play', () => {
      this.playTimeListener =  setInterval(() => { this.increasePlayTime() }, 1000)  
      this.playHistory.push ({ play: $(this.element)[0].currentTime })  
    })

    $(this.element)[0].onpause = () => {
      clearInterval (this.playTimeListener) 
      this.playHistory.push ({ listened : this.playTime })
      this.playHistory.push ({ stop : $(this.element)[0].currentTime })   
      this.playTime = 0  
    }
 
    this.element.addEventListener('volumechange', () => {
       if ( $(this.element).prop('muted') ||  $(this.element)[0].volume == 0 ) {
        this.playHistory.push ({ muted : true })
        this.isMuted = true
       } 
       else if (this.isMuted) {
       this.playHistory.push ({ muted : false }) 
       this.isMuted = false
      }
    }) 
    
    this.element.addEventListener("fullscreenchange", () => {
      this.fullScreen = !this.fullScreen
      this.playHistory.push ({ fullscreen : this.fullScreen })
    });
  } 

  increasePlayTime() {
    ++this.playTime; 
  }
 
  mouseTrackOnly (t) {
    this.setMouseListeners()
    this.followMouseMouvement(t)
  }

  onIntersect() { 
    if (this.args.watchScroll)
    this.watchScroll = setInterval( () => { this.getScrolledPercentage () } , 1000) 
  }

  offIntersect () { 
    clearInterval (this.watchScroll); 
  }
 
  onScreen () { 
    if (this.args.watchTime)
    this.timer =  setInterval(() => { this.increaseWatchTime() }, 1000)   
    this.setMouseListeners() 
    if (this.args.mouseTrack)
    this.followMouseMouvement(3000)  
  } 

  offScreen() { 
    clearInterval (this.timer)  
    this.removeMouseListeners() 
  } 

  watch () {
    this.observerFull.observe(this.element) 
    this.observerIntersect.observe(this.element)
  }

  followMouseMouvement(t) {   
    this.watchMouse = setInterval(() => {  this.recordMouseMouvement() } , t) 
  }

  increaseWatchTime() {  
    if (document.hasFocus())
    ++this.watchTime; 
  }

  recordMouseMouvement(){ 

     let capture = { x: this.mouseX , y : this.mouseY , timestamp : Date.now() }
     if ( capture.x != 0 && capture.y != 0 && this.history.capture.x != capture.x && this.history.capture.y != capture.y ) {
      this.mouseHistory.push(capture)
      this.history.capture.x = capture.x
      this.history.capture.y = capture.y
     }
     
  }

  recordMouseClick(){ 
    let capture = { x: this.clickX , y : this.clickY , timestamp : Date.now()}
    this.mouseClicks.push(capture)
 }

  setMouseListeners() { 
    let offsetm
    let offsetc 
    if (this.args.clickTrack)  
    $(this.element).on('mousedown',(event) => {
        offsetm = $(this.element).offset()
        this.clickX = event.pageX- offsetm.left
        this.clickY = event.pageY- offsetm.top
        this.recordMouseClick()
    });  
    
    if (this.args.mouseTrack)  
    $(this.element).on('mousemove',(event) => { 
      offsetc = $(this.element).offset()
      this.mouseX = event.pageX- offsetc.left
      this.mouseY = event.pageY- offsetc.top
    });   
  }

  removeMouseListeners() {
    $(this.element).off('mousedown')
    $(this.element).off('mousemove')
    clearInterval (this.watchMouse)
  }

  getElementCoords () { 
      let box = this.element.getBoundingClientRect(); 
      let body = document.body;
      let docEl = document.documentElement
      let scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop
      let scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft
      let clientTop = docEl.clientTop || body.clientTop || 0
      let clientLeft = docEl.clientLeft || body.clientLeft || 0
      let top  = box.top +  scrollTop - clientTop
      let left = box.left + scrollLeft - clientLeft
      return { top: Math.round(top), left: Math.round(left) }
  }

  getScrolledPercentage () {
    let articleScrolled = this.ao.scrolled
    let viewPortHeight = $(window).height(); 
    let bottomViewPort = articleScrolled + viewPortHeight 
    let Coords = this.getElementCoords(); 
    let percentage = Math.max( 0, Math.min( (bottomViewPort - Coords.top) * 100 / this.element.getBoundingClientRect().height , 100 )  ) 
    this.scrolledPercentage = Math.max(this.scrolledPercentage , percentage ) 
  }

 

  getInfos(){   
    let events = []

    if ( this.watchTime != 0 )
    events.push( { event : 'watchTime', value : this.watchTime } )

    if ( this.scrolledPercentage != 0 && this.scrolledPercentage != this.history.scroll  ) {
      events.push( { event : 'scroll', value : this.scrolledPercentage } )
      this.history.scroll = this.scrolledPercentage
    }
     
    if ( this.mouseClicks.length != 0  )
    events.push( { event : 'clicks', value : this.mouseClicks } )

    if ( this.mouseHistory.length != 0  )
    events.push( { event : 'mousetrack', value : this.mouseHistory } ) 

    if ( this.playTime != 0)
    events.push( { event : 'playTime', value : this.playTime } ) 

    if ( this.playHistory.length != 0 )
     
    events.push( { event : 'playHistory', value : this.playHistory } ) 

    this.initValues()  
    return events    
  }

  initValues() { 
    this.watchTime = 0           
    this.scrolledPercentage = 0  
    this.mouseClicks = []        
    this.mouseHistory = []   
    this.playTime = 0
    this.playHistory = []    
  }

  gotNews () {   
    if ( this.watchTime != 0 || 
         this.scrolledPercentage != 0 || 
         this.mouseClicks.length != 0 || 
         this.mouseHistory.length != 0  || 
         this.scrolledPercentage != this.history.scroll || 
         this.playTime != 0 || 
         this.playHistory.length != 0 ) {
    return true 
   }
  
    return false 
  }

 
}

module.exports = ElementObserver
