const axios = require('axios')
const endPointURL = 'http://localhost:3000/' //'https://plesk.indepth.inku.be/'
const utils = require("./utils")
const $ = require( "jquery" )

class Sender { 

    constructor (ao,elements) { 
        this.ao = ao 
        this.elements = elements
        this.visitId = null 
        this.lastUrl = window.location.href
        //this.onLeave()
    }

    async handShake () { 
        let visit = await this.ao.handShakeInfos() 
        this.visit = visit 
        /*
        try {
            let resp = await axios.post( endPointURL + 'visit/add' , visit)
            this.visitId =  resp.data._id
        } catch (e) {
            console.log(e)
        }  
        */
    }

    onLeave () {
        window.onbeforeunload = () => { 
            this.heartBeat(document.activeElement.href)
            return undefined
        } 
    }
  
    async heartBeat(nexturl) {
        let behaviors = {
            visitid : this.visitId,
            timestamp : Date.now(),
            articleInfos : this.ao.getInfos(),
            events : [] ,
            nexturl : nexturl || null
        }

        this.elements.map((el) => {
            if (el.gotNews()) { 
                behavior = {
                    element : utils.domElementToObject(el.element),
                    events : el.getInfos()
                }
                if ( behavior.events.length > 0  )
                    behaviors.events.push(behavior)
            }
        }) 
        behaviors._id = this.visit.userid
        behaviors.url = window.location.href
        console.log(behaviors) 

        if (this.lastUrl != window.location.href) {
            this.lastUrl = window.location.href
            resp = await axios.post(endPointURL + 'matomo/updateurl' , 
            {url :this.lastUrl , _id : this.visit.userid, crossDomain: true})
        }

       
        try {
            resp = await axios.post(endPointURL + 'matomo/contenttrack' , 
            {behaviors : JSON.stringify(behaviors) , crossDomain: true})
            behaviors = null ;
        }catch (e) {
            console.log(e)
        } 
    }
}

module.exports = Sender