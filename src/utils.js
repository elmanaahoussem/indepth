// A Bunch of Functions that are useful  
const $ = require( "jquery" )
const md5 = require("md5")
const Cookies = require("js-cookie")


function urlToFileName (url) {
    // Converts a source File url to a filename 
    let arry = url.split('/')
    let filename  = arry[arry.length-1] 
    return filename.substr(0,filename.indexOf('.')).replaceAll('-',' ')
}

exports.domElementToObject = function (element) {
    
    if (element)
    { 
        theElement = {
            tagName : element.tagName,
            trackId : $(element).data('track')
        } 

        if(element.tagName == 'A')
        theElement.href = element.href 

        if(element.tagName == "AUDIO" || element.tagName == "VIDEO") {
            
            theElement.duration = element.duration
            if (element.tagName == "AUDIO") {
                if ( element.getAttribute('src')) 
                    theElement.src =  element.getAttribute('src') 
                else 
                    theElement.src = $(element).find('source')[0].src
            } else {
                theElement.src = $(element).find('source')[0].src
            }  
            theElement.title = urlToFileName(theElement.src)
        }  
        return theElement 
    }
    
    return ({
        tagName : 'Document'
    })

};

exports.getIndepthCookie = function () {   
    return Cookies.get('indepthCookie'); 
}
exports.getMatomoCookie = function () {
    return Cookies.get('_pk_id.2.1fff').split('.')[0]
}

exports.setIndepthCookie = function () { 
    let indepthID = md5(new Date(Date.now()).toString() + ' ' +  Math.random() )  
    Cookies.set('indepthCookie', indepthID, { expires: 360, path: '' });
    return indepthID
}


